package bibleReader.model;

// TODO Add Javadoc comments throughout the class.

/**
 * A simple class that stores the book, chapter number, and verse number.
 * 
 * @author Charles Cusack (Skeleton Code), 2008, edited 2012.
 * @author Daniel Keith (provided the implementation), 2019
 */
public class Reference implements Comparable<Reference> {
	/*
	 * Add the appropriate fields and implement the methods. Notice that there are
	 * no setters. This is intentional since a reference shouldn't change.
	 * Important: You should NOT store the book as a string. There are several
	 * reasons for this that will be clear if you think about it for a few minutes.
	 * There is already a class that you can use instead.
	 */

	private BookOfBible book;
	private int chapter;
	private int verse;

	
	/**
	 * A Constructor for a new Reference.
	 * 
	 * @param book The desired book of the reference.
	 * @param chapter The desired chapter of the reference.
	 * @param verse The desired verse for the reference.
	 */
	public Reference(BookOfBible book, int chapter, int verse) {
		this.book = book;
		this.chapter = chapter;
		this.verse = verse;
	}

	/**
	 * @return The string of the reference's book.
	 */
	public String getBook() {
		return book.toString();
	}

	/**
	 * @return The reference's book of type BookOfBible .
	 */
	public BookOfBible getBookOfBible() {
		return book;
	}

	/**
	 * @return The reference's chapter number.
	 */
	public int getChapter() {
		return chapter;
	}

	/**
	 * @return The reference's verse number.
	 */
	public int getVerse() {
		return verse;
	}

	/**
	 * This method should return the reference in the form: "book chapter:verse" For
	 * instance, "Genesis 2:3".
	 */
	@Override
	public String toString() {
		String returnString;
		returnString = getBook() + " " + getChapter() + ":" + getVerse();
		return returnString;
	}

	/**
	 * This method should compare References. The method signature was originally
	 * written with Object, so I decided to keep it as is and use "instanceof".
	 */
	@Override
	public boolean equals(Object other) {
		if (other instanceof Reference) {
			if (((Reference) other).getBook().equals(getBook()) && ((Reference) other).getChapter() == getChapter()
					&& ((Reference) other).getVerse() == getVerse()) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * This method gives the hashCode of the Reference
	 */
	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	/**
	 * This numerically compares two references. Consult Comparable javadoc for
	 * details. This method works the same way.
	 */
	@Override
	public int compareTo(Reference otherRef) {
		int keyComparison;
		if (getBookOfBible().compareTo(otherRef.getBookOfBible()) != 0) {
			keyComparison = getBookOfBible().compareTo(otherRef.getBookOfBible());
			return keyComparison;
		}
		if ((getChapter() - otherRef.getChapter()) != 0) {
			keyComparison = (getChapter() - otherRef.getChapter());
			return keyComparison;
		}
		if ((getVerse() - otherRef.getVerse()) != 0) {
			keyComparison = (getVerse() - otherRef.getVerse());
			return keyComparison;
		}
		return 0;
	}
}
