package bibleReader.model;

/**
 * A class which stores a verse.
 * 
 * @author Charles Cusack (Skeleton Code), 2008, edited 2012.
 * @author Daniel Keith (provided the implementation), 2019
 */
public class Verse implements Comparable<Verse> {
	private Reference ref;
	private String text;

	/**
	 * Construct a verse given the reference and the text.
	 * 
	 * @param r The reference for the verse
	 * @param t The text of the verse
	 */
	public Verse(Reference r, String t) {
		ref = r;
		text = t;
	}

	/**
	 * Construct a verse given the book, chapter, verse, and text.
	 * 
	 * @param book    The book of the Bible
	 * @param chapter The chapter number
	 * @param verse   The verse number
	 * @param text    The text of the verse
	 */
	public Verse(BookOfBible book, int chapter, int verse, String text) {
		ref = new Reference(book, chapter, verse);
		this.text = text;
	}

	/**
	 * Returns the Reference object for this Verse.
	 * 
	 * @return A reference to the Reference for this Verse.
	 */
	public Reference getReference() {
		return ref;
	}

	/**
	 * Returns the text of this Verse.
	 * 
	 * @return A String representation of the text of the verse.
	 */
	public String getText() {
		return text;
	}

	/**
	 * Returns a String representation of this Verse, which is a String
	 * representation of the Reference followed by the String representation of the
	 * text of the verse.
	 */
	@Override
	public String toString() {
		return ref.toString() + " " + text;
	}

	/**
	 * Should return true if and only if they have the same reference and text.
	 */
	@Override
	public boolean equals(Object other) {
		if (other instanceof Verse) {
			if (((Verse) other).getReference().equals(ref) && ((Verse) other).getText().equals(text)) {
				return true;
			}
			return false;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	/**
	 * From the Comparable interface. See the API for how this is supposed to work.
	 */
	@Override
	public int compareTo(Verse other) {
		int keyComparison;
		if (ref.compareTo(other.getReference()) != 0) {
			keyComparison = ref.compareTo(other.getReference());
			return keyComparison;
		}
		if ((other.getText().hashCode() - text.hashCode()) != 0) {
			keyComparison = (other.getText().hashCode() - text.hashCode());
			return keyComparison;
		}

		return 0;
	}

	/**
	 * Return true if and only if this verse and the other verse have the same
	 * reference. (So the text is ignored).
	 * 
	 * @param other the other Verse.
	 * @return true if and only if this verse and the other have the same reference.
	 */
	public boolean sameReference(Verse other) {
		return getReference().equals(other.getReference());
	}

}
