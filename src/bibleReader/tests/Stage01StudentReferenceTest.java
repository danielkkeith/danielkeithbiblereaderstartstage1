package bibleReader.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bibleReader.model.BookOfBible;
import bibleReader.model.Reference;

/*
 * Tests for the Reference class.
 * @author ?
 */

// Make sure you test at least the following:
// --All of the constructors and all of the methods.
// --The equals for success and for failure, especially when they match in 2 of the 3 places
//    (e.g. same book and chapter but different verse, same chapter and verse but different book, etc.)
// --That compareTo is ordered properly for similar cases as the equals tests.
//    (e.g. should Genesis 1:1 compareTo Revelation 22:21 return a positive or negative number?)
// --Any other potentially tricky things.

public class Stage01StudentReferenceTest {

	// A few sample references to get you started.
	private Reference	ruth1_2;
	private Reference	gen3_4;
	private Reference	rev5_6;
	private Reference	revHashTest;
	private Reference 	genCopy;
	private Reference 	genCopy2;
	/*
	 * Anything that should be done before each test.
	 * For instance, you might create some objects here that are used by several of the tests.
	 */
	@Before
	public void setUp() throws Exception {
		// A few sample references to get you started.
		ruth1_2 = new Reference(BookOfBible.Ruth, 1, 2);
		gen3_4 = new Reference(BookOfBible.Genesis, 3, 4);
		rev5_6 = new Reference(BookOfBible.Revelation, 5, 6);
		revHashTest = rev5_6;
		genCopy = new Reference(BookOfBible.Genesis, 3, 4);
		genCopy2= new Reference(BookOfBible.Genesis, 10, 11);
		
		
		
		// TODO Create more objects that the tests will use here.
		// You need to make them fields so they can be seen in the test methods.
	}

	/*
	 * Anything that should be done at the end of each test.  
	 * Most likely you won't need to do anything here.
	 */
	@After
	public void tearDown() throws Exception {
		// You probably won't need anything here.
	}

	/*
	 * Add as many test methods as you think are necessary. Two suggestions (without implementation) are given below.
	 */
	@Test
	public void testEquals() {
		assertEquals(true, rev5_6.equals(revHashTest));
		assertEquals(true, rev5_6.equals(rev5_6));
		assertEquals(false, rev5_6.equals(gen3_4));
		
		// Constructor Test
		assertEquals(true, gen3_4.equals(genCopy));
		assertEquals(false, gen3_4.equals(genCopy2));
	}
	
	@Test
	public void testHash() {
		assertEquals(rev5_6.hashCode(), revHashTest.hashCode());
		assertEquals(rev5_6.hashCode(), rev5_6.hashCode());
		assertNotEquals(rev5_6.hashCode(), gen3_4.hashCode());
	}

	@Test
	public void testcompareTo() {
		assertEquals(-1, gen3_4.compareTo(rev5_6));
		assertEquals(1, rev5_6.compareTo(gen3_4));
		assertEquals(0, rev5_6.compareTo(revHashTest));
		assertEquals(0, rev5_6.compareTo(rev5_6));
	}
	
	@Test
	public void testGetters() {
		assertEquals("Ruth", ruth1_2.getBook());
		assertEquals("Genesis", gen3_4.getBook());
		assertEquals("Revelation", rev5_6.getBook());

		assertEquals(BookOfBible.Ruth, ruth1_2.getBookOfBible());
		assertEquals(BookOfBible.Genesis, gen3_4.getBookOfBible());
		assertEquals(BookOfBible.Revelation, rev5_6.getBookOfBible());
		
		assertEquals(1, ruth1_2.getChapter());
		assertEquals(3, gen3_4.getChapter());;
		assertEquals(5, rev5_6.getChapter());
		
		assertEquals(2, ruth1_2.getVerse());
		assertEquals(4, gen3_4.getVerse());
		assertEquals(6, rev5_6.getVerse());
	}
	
	@Test
	public void testToString() {
		assertEquals("Ruth 1:2", ruth1_2.toString());
		assertEquals("Genesis 3:4", gen3_4.toString());
		assertEquals("Revelation 5:6", rev5_6.toString());
	}
}
