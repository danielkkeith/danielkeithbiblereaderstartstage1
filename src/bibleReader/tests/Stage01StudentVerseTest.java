package bibleReader.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bibleReader.model.BookOfBible;
import bibleReader.model.Reference;
import bibleReader.model.Verse;


/*
 * Tests for the Verse class.
 * @author Daniel Keith and Grant Williams
 */

public class Stage01StudentVerseTest {
	/*
	 * Anything that should be done before each test.
	 * For instance, you might create some objects here that are used by several of the tests.
	 */
	private Verse ruth1_2;
	private Verse gen3_4;
	
	private Verse equalRuth1_2;
	private Verse equalGen3_4;
	
	private Verse hashRuth1_2;
	private Verse hashGen3_4;
	
	private Reference refRuth1_2;
	private Reference refGen3_4;

	private Verse textRuth1_2;
	
	@Before
	public void setUp() throws Exception {
		// TODO Create objects that the tests will use here.
		// You need to make them fields so they can be seen in the test methods.
		
		// Reference declaration
		refRuth1_2 = new Reference(BookOfBible.Ruth, 1, 2);
		refGen3_4 = new Reference(BookOfBible.Genesis, 3, 4);
		
		// Verse declaration
		ruth1_2 = new Verse(refRuth1_2, "This is the ruth 1 2 verse");
		gen3_4 = new Verse(BookOfBible.Genesis, 3, 4, "This is the gen 3 4 verse");
		
		// Verses that are used in testEquals()
		equalRuth1_2 = new Verse (refRuth1_2, "This is the ruth 1 2 verse");
		equalGen3_4 = new Verse (refGen3_4, "This is the gen 3 4 verse");
		
		// Verses that are used in testHash()
		hashRuth1_2 = ruth1_2;
		hashGen3_4 = gen3_4;
		
		textRuth1_2 = new Verse (refRuth1_2, "This is different text, don't be equal please and thank you");
		
	}
	
	@Test
	public void testGetters() {
		// Tests for both constructors
		assertEquals(refRuth1_2, ruth1_2.getReference());
		assertEquals(refGen3_4, gen3_4.getReference());
		
		// Tests for both constructors
		assertEquals("This is the ruth 1 2 verse", ruth1_2.getText());
		assertEquals("This is the gen 3 4 verse", gen3_4.getText());
	}

	@Test
	public void testToString() {
		assertEquals("Ruth 1:2 This is the ruth 1 2 verse", ruth1_2.toString());
		assertEquals("Genesis 3:4 This is the gen 3 4 verse", gen3_4.toString());
	}
	
	@Test
	public void testEquals() {
		assertEquals(true, ruth1_2.equals(equalRuth1_2));
		assertEquals(false, ruth1_2.equals(gen3_4));
		assertEquals(true, gen3_4.equals(equalGen3_4));
	}
	
	@Test
	public void testHashCode() {
		assertEquals(ruth1_2.hashCode(), hashRuth1_2.hashCode());
		assertEquals(gen3_4.hashCode(), hashGen3_4.hashCode());
		
		assertNotEquals(ruth1_2.hashCode(), gen3_4.hashCode());
		assertNotEquals(ruth1_2.hashCode(), equalRuth1_2.hashCode());
		assertNotEquals(gen3_4.hashCode(), equalGen3_4.hashCode());
	}
	
	@Test
	public void testCompareTo() {
		assertEquals(-1, gen3_4.compareTo(ruth1_2));
		assertEquals(1, ruth1_2.compareTo(gen3_4));
		assertEquals(0, ruth1_2.compareTo(ruth1_2));
		assertEquals(0, ruth1_2.compareTo(equalRuth1_2));
	}
	
	@Test
	public void testSameReference() {
		assertEquals(true, ruth1_2.sameReference(equalRuth1_2));
		assertEquals(true, gen3_4.sameReference(equalGen3_4));
		assertEquals(false, ruth1_2.sameReference(gen3_4));
		assertEquals(false, ruth1_2.sameReference(textRuth1_2));
	}
	/*
	 * Anything that should be done at the end of each test.  
	 * Most likely you won't need to do anything here.
	 */
	@After
	public void tearDown() throws Exception {
		// You probably won't need anything here.
	}

	/*
	 * Add as many test methods as you think are necessary. Two suggestions (without implementation) are given below.
	 */
	
	
}
